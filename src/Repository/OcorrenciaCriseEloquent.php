<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 11/04/18
 * Time: 13:34
 */

namespace PROFIX\Domain\Repository;


use PROFIX\Domain\Model\PROFIX;
use PROFIX\Domain\Model\Contato;

class PROFIXEloquent{

  public function insert($data){
    $new = new PROFIX();
    if(isset($data['id_setor']))
      $new->id_setor = $data['id_setor'];

    if(isset($data['id_cliente']))
      $new->id_cliente = $data['id_cliente'];

    if(isset($data['id_box']))
      $new->id_box = $data['id_box'];

    if(isset($data['tipo_box']))
      $new->tipo_box = $data['tipo_box'];

    if(isset($data['id_operador']))
      $new->id_operador = $data['id_operador'];

    if(isset($data['titulo']))
      $new->titulo = $data['titulo'];

    if(isset($data['obs']))
      $new->obs = $data['obs'];

    if(isset($data['dh_registro']))
      $new->dh_registro = $data['dh_registro'];

    if(isset($data['dh_ini_atend']))
      $new->dh_ini_atend = $data['dh_ini_atend'];

    if(isset($data['dh_fim_atend']))
      $new->dh_fim_atend = $data['dh_fim_atend'];

    if(isset($data['num_input']))
      $new->num_input = $data['num_input'];

    if($new->save()){
      return $new;
    }
    return false;
  }

  public function show($id)
  {
    $ocorrencia = PROFIX::where('ocorrencia_crise.id', $id)
    ->select([
      'ocorrencia_crise.*',
      'cliente.nome_reduzido',
      'cliente.nome_cliente',
    ])
    ->join('cliente', 'cliente.id_cliente', '=', 'ocorrencia_crise.id_cliente')
    ->first();
    $contatos = Contato::contatosByCliente($ocorrencia->id_cliente);
    return ['ocorrencia'=>$ocorrencia, 'contatos'=>$contatos];
  }

  public function getOcorrenciaAberta($idOperador)
  {
    $ocorrenciaAberta = PROFIX::whereNull('dh_fim_atend')
    ->where('id_operador', $idOperador)
    ->first();

    if( isset($ocorrenciaAberta->id) )
      return $ocorrenciaAberta;

    $ocorrencia = PROFIX::whereNull('ocorrencia_crise.id_operador')
      ->whereNull('dh_ini_atend')
      ->orderBy('id', 'ASC')
      ->limit(1)
      ->first();

    if(isset($ocorrencia->id)){
      $PROFIX = PROFIX::find($ocorrencia->id);
      $PROFIX->dh_ini_atend = date('Y-m-d H:i:s');
      $PROFIX->id_operador = $idOperador;
      $PROFIX->save();
    }
    
    return $ocorrencia;
  }

  public function finalizar($data)
  {
    $PROFIX = PROFIX::find($data['id']);
    $PROFIX->dh_fim_atend = date('Y-m-d H:i:s');
    $PROFIX->obs = $data['obs'];
    $PROFIX->save();

    return $ocorrencia;
  }
}