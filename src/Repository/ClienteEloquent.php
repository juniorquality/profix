<?php
namespace PROFIX\Domain\Repository;

use \PROFIX\Domain\Model\Cliente as ClienteModel;

final class ClienteEloquent
{
    protected $defaultRelations = [
        'smartboxes.relays',
        'smartboxes.digitalInputs',
        'smartboxes.sensors',
    ];

    public function getAll()
    {
        return ClienteModel::with($this->defaultRelations)->get();
    }

    public function getById($id)
    {
        return ClienteModel::with($this->defaultRelations)->find($id);
    }
}
