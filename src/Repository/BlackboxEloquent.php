<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 11/04/18
 * Time: 11:24
 */

namespace PROFIX\Domain\Repository;

use PROFIX\Domain\Model\ArBlackbox;
use PROFIX\Domain\Model\Blackbox;

class BlackboxEloquent
{
  protected $defaultRelations;

  public function __construct()
  {
    $this->defaultRelations = [
      'relays',
      'inputs',
      'sensors',
    ];
  }
  public function getById($id)
  {
    return Blackbox::with($this->defaultRelations)->where("id",$id)->first();
  }

  public function getByCodSerie($cod_serie){
    $id_blackbox = Blackbox\DigitalInput::where("cod_serie",$cod_serie)->first()->toArray();
    $id_blackbox = (object)$id_blackbox;
    $id_blackbox = isset($id_blackbox->id_blackbox) ? $id_blackbox->id_blackbox : null;
    return Blackbox::with($this->defaultRelations)->where('id_blackbox', $id_blackbox)->first()->toArray();
  }

  public function getInputsByCodSerie($cod_serie){
    return Blackbox\DigitalInput::with('blackbox')->where("cod_serie",$cod_serie)->first()->toArray();
  }

  public function getInputByCodSerie($cod_serie,$num_input){
    $arr = array(
      "id",
      "id_blackbox",
      "nome_input{$num_input} as nome_input",
      "input{$num_input} as input",
      "sn_invertido{$num_input} as sn_invertido",
      "id_setor{$num_input} as id_setor",
    );
    $reg = Blackbox\DigitalInput::with('blackbox')
      ->where("cod_serie",$cod_serie)
      ->select($arr)
      ->whereRaw("(nome_input{$num_input} LIKE '%panico%' OR nome_input{$num_input} LIKE '%emergencia%')")
      ->first();

    if(isset($reg->id)){
      return $reg->toArray();
    }
    return null;
  }

}