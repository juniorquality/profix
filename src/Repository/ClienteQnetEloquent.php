<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 16/08/17
 * Time: 09:22
 */

namespace PROFIX\Domain\Repository;

use PROFIX\App\Ext\Capsule;
use PROFIX\App\Helper\Utils;
use PROFIX\Domain\Model\ClienteQnet;
use PROFIX\Domain\Model\ClienteQnetRaw;

final class ClienteQnetEloquent {
  /**
   * ClienteQnetEloquent constructor.
   */
  public function __construct() {
    
  }
  
  public function insert(array $data){
    if(Utils::isValidGuid($data['id_imei'])){
      $data['id_projeto'] = 3;
    }else{
      $data['id_projeto'] = 3;
    }
    return ClienteQnet::persist($data);
  }
  
  
  public function fetchByCliente($id_cliente) {
    return ClienteQnet::where("id_cliente",$id_cliente)->with(['qclienteUsuario'])->get()->toArray();
  }
  
  
  public function fetchByQnet($id_qnet){
    return ClienteQnet::where("id_qnet",$id_qnet)->with(['qclienteUsuario','configuracoes'=>function($q){
      return $q->join("roteiro_padrao","roteiro_padrao.id_proc_padrao","=","config_ics_automatico.id_proc_padrao")
        ->where("habilita","S")
        ->select([
          'id_config_ics',
          'qnet',
          'evento_qnet',
          'nome_procedimento',
          'habilita',
          'output',
          'tipo_periodo',
          'periodo_inicio',
          'periodo_fim']);
    },'dines'])->get()->toArray();
  }
  
  public function fetchAll(){
    return ClienteQnet::all()->toArray();
  }
  
  
  public function getKeepAliveQnets(){
    $dt = date("d.m.Y H:i:s");
    /*$keep = Capsule::table(Capsule::raw(
      " (
          SELECT KEEPALIVE_QNET.*
              ,CLIENTE_QNET.NOME
              ,CLIENTE_QNET.ID_CLIENTE
              ,CLIENTE_QNET.ID_CHIP
              ,CLIENTE_QNET.ID_QNET
              ,CLIENTE_QNET.VERSAO
              ,CLIENTE_QNET.MODO
              ,CLIENTE.NOME_REDUZIDO
              ,CLIENTE_QNET.DH_ULT_COMUNICACAO
            ,(
              ROUND(EXTRACT (DAY    FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - KEEPALIVE_QNET.DATA_KEEPALIVE))*24*60*60)+
              ROUND(EXTRACT (HOUR   FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - KEEPALIVE_QNET.DATA_KEEPALIVE))*60*60)+
              ROUND(EXTRACT (MINUTE FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - KEEPALIVE_QNET.DATA_KEEPALIVE))*60)+
              ROUND(EXTRACT (SECOND FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - KEEPALIVE_QNET.DATA_KEEPALIVE)))
            ) AS DELTA_SEGUNDOS
            ,to_char(KEEPALIVE_QNET.DATA_KEEPALIVE,'YYYY-MM-DD HH24:MI:SS') AS DT
        FROM  KEEPALIVE_QNET
        JOIN  CLIENTE_QNET
        ON    CLIENTE_QNET.ID_IMEI = KEEPALIVE_QNET.ID_IMEI
  
        JOIN  CLIENTE
        ON    CLIENTE.ID_CLIENTE = CLIENTE_QNET.ID_CLIENTE
  
        WHERE KEEPALIVE_QNET.ID_KEEPALIVE_QNET IN (
          SELECT  MAX(b.ID_KEEPALIVE_QNET)
          FROM    KEEPALIVE_QNET b
          GROUP BY b.ID_IMEI
        )
      ) a
      "
    ))->select(["a.*"])->get();*/
  
    $keep = Capsule::table(Capsule::raw(
      " (
          SELECT CLIENTE_QNET.NOME
                ,CLIENTE_QNET.ID_CLIENTE
                ,CLIENTE_QNET.ID_CHIP
                ,CLIENTE_QNET.ID_QNET
                ,CLIENTE_QNET.VERSAO
                ,CLIENTE_QNET.MODO
                ,CLIENTE.NOME_REDUZIDO
                ,CLIENTE_QNET.DH_ULT_COMUNICACAO
                ,(
                  ROUND(EXTRACT (DAY    FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO))*24*60*60)+
                  ROUND(EXTRACT (HOUR   FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO))*60*60)+
                  ROUND(EXTRACT (MINUTE FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO))*60)+
                  ROUND(EXTRACT (SECOND FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO)))
                ) AS DELTA_SEGUNDOS
                ,to_char(CLIENTE_QNET.DH_ULT_COMUNICACAO,'YYYY-MM-DD HH24:MI:SS') AS DT
        FROM  CLIENTE_QNET
        
        JOIN  CLIENTE
        ON    CLIENTE.ID_CLIENTE = CLIENTE_QNET.ID_CLIENTE
        
      ) a
      "
    ))->select(["a.*"])->get();
    
    $keep = ClienteQnetRaw::join("CLIENTE","CLIENTE.ID_CLIENTE","=","CLIENTE_QNET.ID_CLIENTE")
      ->select(Capsule::raw(
        "
        CLIENTE_QNET.NOME,
        CLIENTE_QNET.ID_CLIENTE,
        CLIENTE_QNET.ID_CHIP,
        CLIENTE_QNET.ID_QNET,
        CLIENTE_QNET.VERSAO,
        CLIENTE_QNET.MODO,
        CLIENTE_QNET.ID_PROJETO,
        CLIENTE.NOME_REDUZIDO,
        CLIENTE_QNET.DH_ULT_COMUNICACAO,
        (
          ROUND(EXTRACT (DAY    FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO))*24*60*60)+
          ROUND(EXTRACT (HOUR   FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO))*60*60)+
          ROUND(EXTRACT (MINUTE FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO))*60)+
          ROUND(EXTRACT (SECOND FROM (TO_TIMESTAMP ('{$dt}','DD.MM.YYYY HH24:MI:SS')  - CLIENTE_QNET.DH_ULT_COMUNICACAO)))
        ) AS DELTA_SEGUNDOS,
        to_char(CLIENTE_QNET.DH_ULT_COMUNICACAO,'YYYY-MM-DD HH24:MI:SS') AS DT"
      ))->with("saldo")->get();
    return $keep;
    
  }
}