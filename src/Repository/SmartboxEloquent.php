<?php
namespace PROFIX\Domain\Repository;

use \PROFIX\Domain\Model\Smartbox as SmartboxModel;
use \PROFIX\App\Ext\Capsule;

class SmartboxEloquent
{
    protected $defaultRelations;

    public function __construct()
    {
        $this->defaultRelations = [
            'relays',
            'digitalInputs',
            'sensors',
        ];
    }

    public function getAll()
    {
        return SmartboxModel::with($this->defaultRelations)->get();
    }

    public function getById($id)
    {
        return SmartboxModel::with($this->defaultRelations)->find($id);
    }

    public function getBySerial($serial)
    {
        return SmartboxModel::with($this->defaultRelations)->where('serial', $serial)->first();
    }

  public function getInputByIdSbEquipamento($id_sb_equipamento,$num_input){

    $reg = SmartboxModel\DigitalInput::with('smartbox')
      ->where("id_sb_equipamento",$id_sb_equipamento)
      ->where("num_input",$num_input)
      ->whereRaw("(descricao LIKE '%panico%' OR descricao LIKE '%emergencia%')")
      ->first();

    if(isset($reg->id)){
      return $reg->toArray();
    }
    return null;
  }
}
