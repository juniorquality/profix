<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 10:11
 */

namespace PROFIX\Domain\Repository;


use PROFIX\Domain\Model\SetorCliente;
use PROFIX\Domain\Model\SetorClienteOracle;

class SetorClienteEloquent {
  public function insertSetor($data){
    if( isset($data['id_setor_padrao']) && $data['id_setor_padrao'] > 0
        && isset($data['id_cliente']) && $data['id_cliente'] > 0
      ){
      $setorQcv = new SetorCliente();
      $setorQcv->id_cliente = $data['id_cliente'];
      $setorQcv->nome = $data['nome'];
      $setorQcv->sn_ativo = 'S';
      $setorQcv->login = '';
      $setorQcv->dh_registro = date("Y-m-d H:i:s");
      $setorQcv->id_setor_interno = 0;
      $setorQcv->id_setor_padrao = $data['id_setor_padrao'];
      
      if(!$setorQcv->save()){
        return false;
      }
      $id_setor = $setorQcv->id_setor;
      
      $setorOracle = new SetorClienteOracle();
      $setorOracle->id_setor = $id_setor;
      $setorOracle->id_cliente = $data['id_cliente'];
      $setorOracle->nome = $data['nome'];
      $setorOracle->sn_ativo = 'S';
      $setorOracle->login = '';
      $setorOracle->dh_registro = date("Y-m-d H:i:s");
      $setorOracle->id_setor_interno = 0;
      $setorOracle->id_setor_padrao = $data['id_setor_padrao'];
      if(!$setorOracle->save()){
        return false;
      }
      return $id_setor;
    }
    return false;
  }
}