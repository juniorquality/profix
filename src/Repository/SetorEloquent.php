<?php
namespace PROFIX\Domain\Repository;

use \PROFIX\Domain\Model\ProjetoTemplateWizardSetor as ProjetoTemplateWizardSetor;
use \PROFIX\Domain\Model\ProjetoSiteSetorOcorrencia;
use \PROFIX\Domain\Model\ProjetoSiteSetorOcorrenciaChecklist;

use \PROFIX\App\Exception\HttpException;

final class SetorEloquent
{
    public function getSetorsBySite($idSite)
    {
        $setores = ProjetoTemplateWizardSetor::getSetorsBySite($idSite);

        if( count($setores) <= 0 ){
            throw new HttpException(302, ['message' => 'Não há setores cadastrados', 'code' => '302']);
        }
        return $setores;
    }

    public static function getSetorsBySites($idSites)
    {
        $setores = ProjetoTemplateWizardSetor::getSetorsBySites($idSites);
        $setoresSites = [];
        foreach($setores as $v){
          $setoresSites[$v->id_site][] = $v;
        }
        return $setoresSites;
    }

    public static function getSetorBySite($idSites)
    {
        $setores = ProjetoTemplateWizardSetor::getSetorsBySites($idSites);
        return $setores;
    }

    public static function getAllOcorrenciaChecklist($idSetores)
    {
        return ProjetoSiteSetorOcorrenciaChecklist::select([
            'projeto_site_setor_ocorrencia_checklist.id_setor',
            'ocorrencia_checklist.id AS id_ocorrencia_checklist',
            'ocorrencia_checklist.id_prioridade_ocor_check',
            'ocorrencia_checklist.tipo',
            'ocorrencia_checklist.descricao',
        ])->join('ocorrencia_checklist', function ($join) {
            $join->on('ocorrencia_checklist.id', '=', 'projeto_site_setor_ocorrencia_checklist.id_ocorrencia_checklist');
        })
        ->whereIn('projeto_site_setor_ocorrencia_checklist.id_setor', $idSetores)
        ->get();
    }
}
