<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PROFIX\Domain\Repository;
use PROFIX\Domain\Model\SetorClienteContato;
use PROFIX\Domain\Model\SetorClienteContatoOracle;

/**
 * Description of SetorClienteContatoEloquent
 *
 * @author quality
 */
class SetorClienteContatoEloquent {
  //put your code here
  public function insertSetorClienteContato($data){
    $setorClienteContato = new SetorClienteContato();
    $setorClienteContato->id_setor = $data['id_setor'];
    $setorClienteContato->id_cliente = $data['id_cliente'];
    $setorClienteContato->id_clientes_contatos = $data['id_clientes_contatos'];
    $setorClienteContato->sn_ativo = 'S';
    $setorClienteContato->usuario = "";
    $setorClienteContato->dh_registro = date("Y-m-d H:i:s");

    if($setorClienteContato->save()){
      $setorClienteContatoOracle = new SetorClienteContatoOracle();
      $setorClienteContatoOracle->id = $setorClienteContato->id;
      $setorClienteContatoOracle->id_setor = $data['id_setor'];
      $setorClienteContatoOracle->id_cliente = $data['id_cliente'];
      $setorClienteContatoOracle->id_clientes_contatos = $data['id_clientes_contatos'];
      $setorClienteContatoOracle->sn_ativo = 'S';
      $setorClienteContatoOracle->usuario = "";
      $setorClienteContatoOracle->dh_registro = date("Y-m-d H:i:s");
      $setorClienteContatoOracle->save();
      
    }else{
      return false;
    }
    
    return $setorClienteContato->id;
  }
}
