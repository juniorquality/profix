<?php
namespace PROFIX\Domain\Repository;

use \PROFIX\Domain\Model\Usuario as UsuarioModel;
use \PROFIX\Domain\Model\ProjetoTemplateWizardSetor as ProjetoTemplateWizardSetor;
use \PROFIX\Domain\Model\Projeto;
use \PROFIX\Domain\Model\ProjetoCargo;
use \PROFIX\Domain\Model\ProjetoOcorrenciaPadrao;
use \PROFIX\Domain\Model\OcorrenciaChecklist;
use \PROFIX\Domain\Model\SetorPadrao;
use \PROFIX\Domain\Repository\ChecklistSetorEloquent;
use \PROFIX\Domain\Repository\SetorEloquent;
use \PROFIX\App\Exception\HttpException;


final class LoginEloquent
{
    public function login($login, $password)
    {
        $password = md5($password);
        $userLogin = UsuarioModel::getLoginByPass($login, $password);

        if( !isset($userLogin->id_usuario) ){
          return false;
        }

        return $userLogin;
    }
}
