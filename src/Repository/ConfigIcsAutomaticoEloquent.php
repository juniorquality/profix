<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 16/08/17
 * Time: 14:14
 */

namespace PROFIX\Domain\Repository;

use PROFIX\App\Ext\Capsule;
use PROFIX\App\Helper\Utils;
use PROFIX\Domain\Model\ConfigIcsAutomatico;

class ConfigIcsAutomaticoEloquent {
  public static function fetchDesbloqueiosByQnet($id_qnet, $id_cliente) {
    $select = array();
    $select[]= "distinct ca.id_mosaico_config,";
    $select[]= "ca.tipo_evento,";
    $select[]= "
            (
            	select id_config_ics
            	from config_ics_automatico
            	where output=ca.output
            	and id_mosaico_config=ca.id_mosaico_config
            	and tipo_evento=ca.tipo_evento
            	and rownum=1
            ) as ID_CONFIG_ICS,
        ";

    $select[]= "ca.output,";
    $select[]= "NVL(dbms_lob.substr((CASE";
    $select[]= "WHEN SUBSTR(ca.output,1,3)='0-1' THEN SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O1')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O1')),'N= ')+3,INSTR(SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O1')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O1')),'N= ')+3),'/>')-2)";
    $select[]= "WHEN SUBSTR(ca.output,1,3)='0-2' THEN SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O2')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O2')),'N= ')+3,INSTR(SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O2')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O2')),'N= ')+3),'/>')-2)";
    $select[]= "WHEN SUBSTR(ca.output,1,3)='0-3' THEN SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O3')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O3')),'N= ')+3,INSTR(SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O3')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O3')),'N= ')+3),'/>')-2)";
    $select[]= "WHEN SUBSTR(ca.output,1,3)='0-4' THEN SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O4')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O4')),'N= ')+3,INSTR(SUBSTR(SUBSTR(mc.config, INSTR(mc.config,'<O4')), INSTR(SUBSTR(mc.config, INSTR(mc.config,'<O4')),'N= ')+3),'/>')-2)";
    $select[]= "END), 4000,1),(select nome from blackbox_relay where to_char(id)=SUBSTR(ca.output,1,INSTR(ca.output,'-')-1) and rownum=1)";
    $select[]= ") as  NOME_RELAY,";
    $select[]= "ca.habilita";
    $tipoEvento = "Output";

    $listaDesbloqueio = ConfigIcsAutomatico::from( 'CONFIG_ICS_AUTOMATICO ca' )
      ->join("roteiro_padrao rp","rp.id_proc_padrao","=","ca.id_proc_padrao")
      ->join("mosaico_config mc","ca.id_mosaico_config","=","mc.id_mosaico_config")
      ->join("cliente c","rp.id_cliente","=","c.id_cliente")
      ->whereRaw("ca.tipo_evento LIKE '%{$tipoEvento}%'")
      ->whereRaw("mc.id_mosaico_config in (
            SELECT  j.id_mosaico_config
            FROM    config_ics_automatico j
            WHERE		j.habilita = 'S'
            AND     j.qnet = '{$id_qnet}'
			)")
      ->whereRaw("rp.id_cliente = '{$id_cliente}'")
      ->whereRaw("ca.HABILITA = 'S'")
      ->select(Capsule::raw(implode(" ",$select)));
    $listaDesbloqueioTmp = $listaDesbloqueio->get()->toArray();

    $listaDesbloqueio = [];
    foreach($listaDesbloqueioTmp as $item){
      if(stripos($item['tipo_evento'],"SmartBox")!==false){
        $item['nome_relay'] = Utils::getNomeRelaySB($item['output']);
      }
      $listaDesbloqueio[] = $item;
    }
    return $listaDesbloqueio;
  }
}
