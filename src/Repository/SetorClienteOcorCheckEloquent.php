<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 10:11
 */

namespace PROFIX\Domain\Repository;


use PROFIX\Domain\Model\SetorCliente;
use PROFIX\Domain\Model\SetorClienteOcorCheck;
use PROFIX\Domain\Model\SetorClienteOcorCheckOracle;

class SetorClienteOcorCheckEloquent {
  public function insertOcorrenciaCliente($data){
    
    $ocorCheck = new SetorClienteOcorCheck();
    $ocorCheck->id_setor = $data['id_setor'];
    $ocorCheck->id_cliente = $data['id_cliente'];
    $ocorCheck->id_ocorrencia_checklist = $data['id_ocorrencia_checklist'];
    $ocorCheck->sn_ativo = 'S';
    $ocorCheck->usuario = "";
    $ocorCheck->dh_registro = date("Y-m-d H:i:s");
    if(!$ocorCheck->save()){
      return false;
    }

    $id = $ocorCheck->id;
    $ocorCheckOracle = new SetorClienteOcorCheckOracle();
    $ocorCheckOracle->id = $id;
    $ocorCheckOracle->id_setor = $data['id_setor'];
    $ocorCheckOracle->id_cliente = $data['id_cliente'];
    $ocorCheckOracle->id_ocorrencia_checklist = $data['id_ocorrencia_checklist'];
    $ocorCheckOracle->sn_ativo = 'S';
    $ocorCheckOracle->usuario = "";
    $ocorCheckOracle->dh_registro = date("Y-m-d H:i:s");
    if(!$ocorCheckOracle->save()){
      return false;
    }
    return $id;
    
  }
}