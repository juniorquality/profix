<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 11:33
 */

namespace PROFIX\Domain\Model;


class ClientesContatoOracle extends AbstractOracleModel{
  protected $connection = 'oracle_main';
  protected $table = 'CLIENTES_CONTATOS';
  protected $primaryKey = 'ID';
  public $timestamps = false;
  
}