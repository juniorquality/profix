<?php
namespace PROFIX\Domain\Model;

use \PROFIX\App\Ext\Capsule;

final class SetorPadrao extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'setor_padrao';
    protected $primaryKey = 'id';
    protected $timestamp = false;
    protected $hidden = ['pivot'];

    public function ocorrencias()
    {
        return $this->belongsToMany(
            OcorrenciaChecklist::class,
            'setor_padrao_ocorrencia_checklist',
            'id_setor_padrao',
            'id_ocorrencia_checklist'
        )
        ->select([
            'ocorrencia_checklist.id',
            'ocorrencia_checklist.tipo',
            'ocorrencia_checklist.id_prioridade_ocor_check',
            'ocorrencia_checklist.descricao',
        ])
        ->where('ocorrencia_checklist.sn_ativo', 'S');
    }
}
