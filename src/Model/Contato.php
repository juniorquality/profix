<?php
namespace PROFIX\Domain\Model;

use \PROFIX\Domain\Model\ClienteQnet;
use \PROFIX\Domain\Model\Smartbox;
use \PROFIX\Domain\Model\SmartboxCliente;

final class Contato extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'clientes_contatos';
    protected $primaryKey = 'id';

    public function contatosByCliente($idCliente)
    {
        return self::where([
            'id_cliente'=>$idCliente,
        ])
        ->where('telefone','!=', '')
        ->select([
          'id',
          'id_cargo',
          'nome',
          'ddd',
          'telefone',
          'prioridade',
        ])
        ->orderBy('prioridade','nome')
        ->get();
    }
}
