<?php
namespace PROFIX\Domain\Model;

final class Setor extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'setor';
    protected $primaryKey = 'id_setor';
}
