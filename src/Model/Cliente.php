<?php
namespace PROFIX\Domain\Model;

use \PROFIX\Domain\Model\ClienteQnet;
use \PROFIX\Domain\Model\Smartbox;
use \PROFIX\Domain\Model\SmartboxCliente;

final class Cliente extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'cliente';
    protected $primaryKey = 'id_cliente';

    public function qnets()
    {
        return $this->hasMany(ClienteQnet::class, 'id_cliente', 'id_cliente');
    }

    public function smartboxes()
    {
        return $this->hasManyThrough(Smartbox::class, SmartboxCliente::class, 'id_cliente', 'id');
    }
}
