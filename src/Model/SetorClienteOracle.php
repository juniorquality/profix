<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 10:04
 */

namespace PROFIX\Domain\Model;
use \PROFIX\App\Ext\Capsule;

class SetorClienteOracle extends AbstractModel{
  protected $connection = 'oracle_main';
  protected $table = 'setor_cliente';
  protected $primaryKey = 'id_setor';
  public $timestamps = false;
  
}