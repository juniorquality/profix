<?php
namespace PROFIX\Domain\Model;

final class OcorrenciaChecklist extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'ocorrencia_checklist';
    protected $primaryKey = 'id';
    protected $hidden = ['pivot'];
}
