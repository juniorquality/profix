<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 10:10
 */

namespace PROFIX\Domain\Model;


class SegmentoSetor extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'vw_agrupamento_segmento_setor_padrao';
  protected $primaryKey = null;
  public $timestamps = false;
  
}