<?php
  /**
   * Created by PhpStorm.
   * User: quality
   * Date: 16/01/18
   * Time: 14:45
   */
  
  namespace PROFIX\Domain\Model;
  
  
  class QclientFuncionalidade extends AbstractModel{
    protected $connection = 'oracle_main';
    public $table = 'QCLIENT_FUNCIONALIDADE';
    protected $primaryKey = 'ID';
    public $timestamps = false;
    protected $softDelete = false;
    public $sequence = "QCLIENT_FUNCIONALIDADE_ID_SEQ";
  
  }