<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 14/09/17
 * Time: 15:12
 */

namespace PROFIX\Domain\Model;


class ArBlackbox extends AbstractModel{
  protected $connection = 'mysql_bb';
  protected $table = 'ar_blackbox';
  //protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  
}