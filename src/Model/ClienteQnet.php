<?php
namespace PROFIX\Domain\Model;
use \PROFIX\App\Ext\Capsule;
use PROFIX\Domain\Repository\ConfigIcsAutomaticoEloquent;

final class ClienteQnet extends AbstractModel
{
  protected $connection = 'oracle_main';
  protected $table = 'cliente_qnet';
  protected $primaryKey = 'id_qnet';
  public $timestamps = false;
  protected $appends = ['ocorrencias_qnet','so','tipo_dispositivo','desbloqueios'];
  
  public function cliente(){
    return $this->hasOne('PROFIX\Domain\Model\Cliente', 'id_cliente', 'id_cliente');
  }
  
  public function ocorrenciaqnet(){
    return $this->hasMany('PROFIX\Domain\Model\OcorrenciaQnet','id_qnet')->orderBy("data",'desc');
  }
  
  public function qclienteusuario(){
    return $this->hasOne('PROFIX\Domain\Model\QclienteUsuarios','id_qnet','id_qnet');
  }

  public function getSoAttribute(){
    return $this->id_projeto < 3 ? "ANDROID" : "IOS";
  }
  
  public function getTipoDispositivoAttribute(){
    return $this->id_projeto > 2 ? "QAPP" : "QNET";
  }
  
  public function configuracao(){
    return $this->hasMany('PROFIX\Domain\Model\ConfigIcsAutomatico','qnet','id_qnet');
  }
  
  public function getDesbloqueiosAttribute(){
    return ConfigIcsAutomaticoEloquent::fetchDesbloqueiosByQnet($this->id_qnet,$this->id_cliente);
  }
  
  public static function getByIMEI($imei){
    $qnet = self::where("ID_IMEI",$imei)->first();
    return $qnet;
  }
  
  public static function getById($imei){
    $qnet = self::where("ID_QNET",$imei)->first();
    return $qnet;
  }
  
}
