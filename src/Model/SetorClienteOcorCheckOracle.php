<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 15:24
 */

namespace PROFIX\Domain\Model;


class SetorClienteOcorCheckOracle extends AbstractOracleModel {
  protected $connection = 'oracle_main';
  protected $table = 'setor_cliente_ocor_check';
  protected $primaryKey = 'id';
  public $timestamps = false;
  
}