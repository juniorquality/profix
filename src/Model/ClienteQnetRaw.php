<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 22/09/17
 * Time: 08:55
 */

namespace PROFIX\Domain\Model;


class ClienteQnetRaw extends AbstractOracleModel
{
  protected $connection = 'oracle_main';
  protected $table = 'cliente_qnet';
  protected $primaryKey = 'id_qnet';
  public $sequence = 'sgo.CLIENTE_QNETID';
  protected $appends = ['so','tipo_dispositivo'];
  
  public function getSoAttribute(){
    return $this->id_projeto < 3 ? "ANDROID" : "IOS";
  }
  
  public function getTipoDispositivoAttribute(){
    return $this->id_projeto > 1 ? "QAPP" : "QNET";
  }
  
}