<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 11/04/18
 * Time: 11:08
 */

namespace PROFIX\Domain\Model;


class PROFIX extends AbstractOracleModel{
  protected $connection = 'oracle_main';
  public $table = 'OCORRENCIA_CRISE';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  public $sequence = "sgo.OCORRENCIA_CRISE_ID_SEQ";
}