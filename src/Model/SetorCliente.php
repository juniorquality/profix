<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 10:10
 */

namespace PROFIX\Domain\Model;


class SetorCliente extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'setor_cliente';
  protected $primaryKey = 'id_setor';
  public $timestamps = false;
  
}