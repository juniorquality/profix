<?php
namespace PROFIX\Domain\Model;

final class Usuario extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';

    public static function getLoginByPass($login, $password) {
        return self::where([
            'email'=>$login,
            'senha'=>$password,
        ])->first();
    }
}
