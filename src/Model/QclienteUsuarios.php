<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 16/08/17
 * Time: 10:38
 */

namespace PROFIX\Domain\Model;


class QclienteUsuarios extends AbstractModel{
  protected $connection = 'oracle_main';
  public $table = 'QCLIENT_USUARIOS';
  protected $primaryKey = 'ID';
  public $timestamps = false;
  protected $softDelete = false;
  public $sequence = "QCLIENT_USUARIOS_ID_SEQ";
  
}