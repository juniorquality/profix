<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 18/08/17
 * Time: 13:31
 */

namespace PROFIX\Domain\Model\Blackbox;
use \PROFIX\Domain\Model\AbstractModel;
use PROFIX\Domain\Model\Blackbox;

class DigitalInput extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'blackbox_input';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;

  public function blackbox(){
    return $this->hasOne(Blackbox::class, 'id_blackbox', 'id_blackbox');
  }
  
}