<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 18/08/17
 * Time: 13:36
 */

namespace PROFIX\Domain\Model\Blackbox;
use \PROFIX\Domain\Model\AbstractModel;

class Sensor extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'blackbox_sensor';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  
  public function log(){
    //return $this->hasMany(SensorLog::class,'id','id_ss')->where('dia',date("Y-m-d"))->orderBy('hora','ASC');
    return $this->hasMany(SensorLog::class,'id_ss','id')->orderBy('data','DESC')->limit(10);
  }
}
