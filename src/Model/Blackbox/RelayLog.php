<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 18/08/17
 * Time: 13:23
 */

namespace PROFIX\Domain\Model\Blackbox;
use PROFIX\Domain\Model\Blackbox;
use \PROFIX\Domain\Model\AbstractModel;
use \PROFIX\App\Ext\Capsule;


class RelayLog extends AbstractModel{
  protected $connection = 'mysql_main';
  protected $table = 'blackbox_out';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
}