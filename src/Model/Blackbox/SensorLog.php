<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 18/08/17
 * Time: 13:39
 */

namespace PROFIX\Domain\Model\Blackbox;

use PROFIX\Domain\Model\AbstractModel;

class SensorLog extends AbstractModel{
  protected $connection = 'mysql_main';
  protected $table = 'blackbox_temperatura_log';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
}