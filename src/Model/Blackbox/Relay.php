<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 18/08/17
 * Time: 13:18
 */

namespace PROFIX\Domain\Model\Blackbox;

use PROFIX\Domain\Model\Blackbox;
use \PROFIX\Domain\Model\AbstractModel;
use \PROFIX\App\Ext\Capsule;

class Relay extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'blackbox_relay';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  
  public function log(){
    return $this->hasMany(RelayLog::class,'id_relay','id_relay')->orderBy('data','DESC')->limit(10);
  }
}