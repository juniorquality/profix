<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 14:39
 */

namespace PROFIX\Domain\Model;


class SetorClienteContatoOracle extends AbstractOracleModel {
  protected $connection = 'oracle_main';
  protected $table = 'SETOR_CLIENTE_CONTATO';
  protected $primaryKey = 'ID';
  public $timestamps = false;
  
}