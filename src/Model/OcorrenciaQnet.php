<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 16/08/17
 * Time: 09:50
 */

namespace PROFIX\Domain\Model;

final class OcorrenciaQnet extends AbstractModel{
  protected $connection = 'oracle_main';
  public $table = 'OCORRENCIAS_QNET';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  public $sequence = "sgo.OCORRENCIAS_QNETID";
}