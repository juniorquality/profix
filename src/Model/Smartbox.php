<?php
namespace PROFIX\Domain\Model;

use \PROFIX\Domain\Model\Smartbox\DigitalInput;
use \PROFIX\Domain\Model\Smartbox\Relay;
use \PROFIX\Domain\Model\Smartbox\Sensor;

final class Smartbox extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_equipamento';
    protected $primaryKey = 'id';

    public function digitalInputs()
    {
        return $this->hasMany(DigitalInput::class, 'id_sb_equipamento');
    }

    public function relays()
    {
        return $this->hasMany(Relay::class, 'id_sb_equipamento');
    }

    public function sensors()
    {
        return $this->hasMany(Sensor::class, 'id_sb_equipamento');
    }

    public function cliente()
    {
      return $this->hasOne(SmartboxCliente::class, 'id_sb_equipamento');
    }
}
