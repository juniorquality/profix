<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 15/09/17
 * Time: 10:20
 */

namespace PROFIX\Domain\Model;


class ArSinalGsm extends AbstractModel{
  protected $connection = 'mysql_bb';
  protected $table = 'ar_sinal_gsm';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  
  public function blackbox(){
    return $this->hasOne(ArBlackbox::class,"cod_serie","cod_serie");
  }
  
}