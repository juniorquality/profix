<?php
namespace PROFIX\Domain\Model;

class Agenda extends AbstractModel
{
    protected $connection = 'mssql_main';
    protected $table = 'Agenda';
    protected $primaryKey = 'IdAviso';
    public $timestamps = false;
}
