<?php
namespace PROFIX\Domain\Model;

use \PROFIX\App\Ext\Capsule;

abstract class AbstractOracleModel extends AbstractModel
{
  public static function getSeqUniq()
  {
    $sequence = Capsule::getSequence();
    $seq = new static();
    return $sequence->nextValue($seq->sequence);
  }
}

