<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 14:39
 */

namespace PROFIX\Domain\Model;

class SetorClienteContato extends AbstractModel {
  protected $connection = 'mysql_main';
  protected $table = 'setor_cliente_contato';
  protected $primaryKey = 'id';
  public $timestamps = false;
  
}