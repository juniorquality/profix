<?php
namespace PROFIX\Domain\Model;

final class SmartboxCliente extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_equipamento_cliente';
    protected $primaryKey = 'id';

    public function cliente()
    {
        return $this->hasOne('PROFIX\Domain\Model\Cliente', 'id_cliente', 'id_cliente');
    }
}
