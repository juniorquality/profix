<?php
namespace PROFIX\Domain\Model;

use \PROFIX\App\Ext\Capsule;

final class SetorPadraoOcorrenciaChecklist extends AbstractModel
{
    protected $connection = 'mysql_main';
    protected $table = 'setor_padrao_ocorrencia_checklist';
    protected $primaryKey = 'id';
    protected $timestamp = false;
    protected $hidden = ['pivot'];
}
