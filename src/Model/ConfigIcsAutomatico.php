<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 16/08/17
 * Time: 13:57
 */

namespace PROFIX\Domain\Model;


class ConfigIcsAutomatico extends AbstractModel {
  protected $connection = 'oracle_main';
  public $table = 'CONFIG_ICS_AUTOMATICO';
  protected $primaryKey = 'ID_CONFIG_ICS';
  public $timestamps = false;
  protected $softDelete = false;
  public $sequence = "sgo.SEQ_ID_CONFIG_ICS";
  
}