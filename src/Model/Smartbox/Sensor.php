<?php
namespace PROFIX\Domain\Model\Smartbox;

use \PROFIX\Domain\Model\Smartbox;
use \PROFIX\Domain\Model\AbstractModel;

class Sensor extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_sensor';
    protected $primaryKey = 'id';

    public function smartbox()
    {
        return $this->belongsTo(Smartbox::class, 'id', 'id_sb_equipamento');
    }
}
