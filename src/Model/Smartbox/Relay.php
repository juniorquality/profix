<?php
namespace PROFIX\Domain\Model\Smartbox;

use \PROFIX\Domain\Model\Smartbox;
use \PROFIX\Domain\Model\AbstractModel;
use \PROFIX\App\Ext\Capsule;

class Relay extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_relay';
    protected $primaryKey = 'id';

    public function smartbox()
    {
        return $this->belongsTo(Smartbox::class, 'id', 'id_sb_equipamento');
    }
}
