<?php
namespace PROFIX\Domain\Model\Smartbox;

use \PROFIX\Domain\Model\AbstractModel;

class RelayLog extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_relay_log';
    protected $primaryKey = 'id';
}
