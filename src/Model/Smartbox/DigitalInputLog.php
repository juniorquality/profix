<?php
namespace PROFIX\Domain\Model\Smartbox;

use \PROFIX\Domain\Model\AbstractModel;

class DigitalInputLog extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_input_log';
    protected $primaryKey = 'id';
}
