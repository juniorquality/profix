<?php
namespace PROFIX\Domain\Model\Smartbox;

use \PROFIX\Domain\Model\AbstractModel;

class DigitalInput extends AbstractModel
{
    protected $connection = 'mysql_sb';
    protected $table = 'sb_input';
    protected $primaryKey = 'id';

    public function smartbox()
    {
        return $this->hasOne('\PROFIX\Domain\Model\Smartbox', 'id', 'id_sb_equipamento')->with('cliente');
    }
}
