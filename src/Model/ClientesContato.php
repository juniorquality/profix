<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 06/12/17
 * Time: 11:37
 */

namespace PROFIX\Domain\Model;


class ClientesContato extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'clientes_contatos';
  protected $primaryKey = 'id';
  public $timestamps = false;
  
}