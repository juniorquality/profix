<?php
namespace PROFIX\Domain\Model;

use PROFIX\Domain\Model\Blackbox\DigitalInput;
use PROFIX\Domain\Model\Blackbox\Relay;
use PROFIX\Domain\Model\Blackbox\Sensor;
use PROFIX\Domain\Model\Cliente;

final class Blackbox extends AbstractModel
{
  protected $connection = 'mysql_main';
  protected $table = 'blackbox';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $softDelete = false;
  protected $appends = ['cod_serie'];
  public function cliente(){
    return $this->hasOne(Cliente::class, 'id_cliente', 'id_cliente');
  }
  public function relays(){
    return $this->hasMany(Relay::class, 'id_blackbox','id_blackbox');
  }
  public function inputs(){
    return $this->hasMany(DigitalInput::class, 'id_blackbox','id_blackbox');
  }
  public function sensors(){
    return $this->hasMany(Sensor::class, 'id_blackbox','id_blackbox');
  }
  
  public function getCodSerieAttribute(){
    $arbb = ArBlackbox::where('id',$this->id_blackbox)->select(["cod_serie"])->first();
    return $arbb->cod_serie;
  }
}
