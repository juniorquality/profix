<?php
$container['repository.cliente'] = function () {
    return new \PROFIX\Domain\Repository\ClienteEloquent();
};

$container['repository.smartbox'] = function () {
    return new \PROFIX\Domain\Repository\SmartboxEloquent();
};

$container['repository.blackbox'] = function () {
  return new \PROFIX\Domain\Repository\BlackboxEloquent();
};

$container['repository.clienteQnet'] = function () {
    return new \PROFIX\Domain\Repository\ClienteQnetEloquent();
};

$container['repository.login'] = function () {
    return new \PROFIX\Domain\Repository\LoginEloquent();
};

$container['repository.setor'] = function () {
    return new \PROFIX\Domain\Repository\SetorEloquent();
};


$container['repository.setor_cliente_contato'] = function () {
    return new \PROFIX\Domain\Repository\SetorClienteContatoEloquent();
};

$container['repository.setor_cliente'] = function () {
    return new \PROFIX\Domain\Repository\SetorClienteEloquent();
};

$container['repository.setor_cliente_ocor_check'] = function () {
    return new \PROFIX\Domain\Repository\SetorClienteOcorCheckEloquent();
};

$container['repository.ocorrencia_crise'] = function () {
  return new \PROFIX\Domain\Repository\PROFIXEloquent();
};