<?php
namespace PROFIX\App\Middleware;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use \PROFIX\App\Exception\HttpException;
use \Slim\Http\Response;
use \Slim\Http\Body;

class JsonResponse
{
  protected $start = 0;

  public function __construct()
  {
    $this->start = microtime(true);
  }

  public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
  {

    try {
      $response = $next($request, $response);
      $statusCode = $response->getStatusCode();
      $body = $response->getBody();
      $data = json_decode($body);

      $statusCode = isset($data->statusCode) ? $data->statusCode : $statusCode;


      if (empty($data)) {
        return $response;
      }

      $data = ['data' => $data];
      $response = $response->withBody(new Body(fopen('php://temp', 'r+')));
      return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus($statusCode)
        ->write(json_encode(array_merge(['total_time' => $this->elapsed()], (array)$data)));
    } catch (HttpException $http) {
      $data = (array) json_decode($http->getMessage());
      $data = array_merge(['total_time' => $this->elapsed()], $data);
      return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus($http->getStatusCode())
        ->write(json_encode($data));
    } catch (\Exception $e) {
      return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(500)
        ->write(json_encode([
          'total_time' => $this->elapsed(),
          'error' => $e->getMessage()
        ]));
    }
  }

  public function elapsed()
  {
    return round(microtime(true) - $this->start, 5);
  }
}
