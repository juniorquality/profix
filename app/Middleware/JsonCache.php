<?php
namespace PROFIX\App\Middleware;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use \Slim\Http\Response;
use \Slim\Http\Body;

define('CACHE_PATH', __DIR__ . '/cache');
define('CACHE_TTL', 1);

class JsonCache
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $cacheHash = hash('sha256', json_encode($request->getAttribute('routeInfo')['request']));
        $cacheFile = CACHE_PATH . '/' . $cacheHash . '.json';

        $data = null;
        if (file_exists($cacheFile) && is_readable($cacheFile)) {
            $cacheLive = microtime(true) - filemtime($cacheFile);
            if ($cacheLive < CACHE_TTL) {
                $data = json_decode(file_get_contents($cacheFile));
            }
        }

        if (!empty($data)) {
            $response = $response->withBody(new Body(fopen('php://temp', 'r+')));
            $response->write(json_encode($data));
            return $response;
        }

        $response = $next($request, $response);
        $body = $response->getBody();
        $data = json_decode($body);

        if (empty($data)) return $response;

        $data = json_encode($data);
        $response = $response->withBody(new Body(fopen('php://temp', 'r+')));
        $response->write($data);

        file_put_contents($cacheFile, $data);

        return $response;
    }
}
