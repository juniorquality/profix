<?php
$app->group('', function () use ($app) {
  $app->get('/', '\PROFIX\App\Controller\HomeController:Index');
  $app->get('/home', '\PROFIX\App\Controller\HomeController:Index');
  $app->get('/teste', '\PROFIX\App\Controller\HomeController:teste');
  $app->get('/sair', '\PROFIX\App\Controller\HomeController:Sair');


  $app->group('/ocorrencia', function () use ($app) {
    $app->get('/{id:[0-9]+}', '\PROFIX\App\Controller\PROFIXController:Index');

    $app->get('/tratar', '\PROFIX\App\Controller\PROFIXController:Abertas')
    ->add(new \PROFIX\App\Middleware\JsonResponse());

    $app->post('/save', '\PROFIX\App\Controller\PROFIXController:save')
    ->add(new \PROFIX\App\Middleware\JsonResponse());

    $app->post('/finalizar', '\PROFIX\App\Controller\PROFIXController:Finalizar')
    ->add(new \PROFIX\App\Middleware\JsonResponse());
  });

  $app->get('/login', '\PROFIX\App\Controller\LoginController:Index');
  $app->post('/login', '\PROFIX\App\Controller\LoginController:doLogin');

  $app->get('/ocorrencia/save', '\PROFIX\App\Controller\PROFIXController:save')
    ->add(new \PROFIX\App\Middleware\JsonResponse());

});
// ->run();
// ->add(new \PROFIX\App\Middleware\JsonResponse());

$app->group('api/v1', function () use ($app) {
  $app->get('/', '\PROFIX\App\Controller\HomeController:Index');
});