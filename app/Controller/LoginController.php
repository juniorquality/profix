<?php
namespace PROFIX\App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
// use \PROFIX\Domain\Model\Agenda;

class LoginController extends BaseController
{
    public function index(Request $request, Response $response, $args)
    {
      $exists = $this->container['session']->exists('usuario');
      if($exists === true) {
          return $response->withStatus(200)->withHeader('Location', 'home');
      }
      return $this->container['view']->render($response, 'login/index.twig.php', []);
    }

    public function doLogin(Request $request, Response $response, $args)
    {
      $data = $request->getParsedBody();
      $login = $this->repository('login')->login($data['login'],$data['senha']);

      if($login === false){
        $this->container['flash']->addMessage('message', 'Usuario não encontrado.');
        return $response->withStatus(302)->withHeader('Location', 'login');
      }

      $login->dataLogin = date('YmdHis');

      $this->container['session']->set('usuario', $login);
      return $response->withStatus(200)->withHeader('Location', 'home');

    }
}
