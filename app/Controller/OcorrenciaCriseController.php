<?php
/**
 * Created by PhpStorm.
 * User: Jonas Ferreira
 * Date: 11/04/18
 * Time: 11:10
 */

namespace PROFIX\App\Controller;

use PROFIX\App\Helper\Utils;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Symfony\Component\Console\Helper\Helper;
use PROFIX\App\Exception\HttpException;

// use \PROFIX\Domain\Model\Agenda;
class PROFIXController extends BaseController{

  protected function processBB($data){
    $input = $this->repository("blackbox")->getInputByCodSerie($data['id'],$data['num_input']);
    if($input!=null){
      $active = $input['sn_invertido'] == 'S' ? 0 : 1;
      $valor = $data['valor'] * 1;
      if($active==$valor){
        $insert = [
          "id_cliente"=>$input['blackbox']['id_cliente'],
          "id_box"=>$input['blackbox']['id'],
          "tipo_box"=>$data['tipo'],
          "titulo"=>"DISPARO DE {$input['nome_input']}",
          "obs"=>null,
          "dh_registro"=>date("Y-m-d H:i:s"),
          'num_input'=>$data['num_input']
        ];
        $input = $this->repository('ocorrencia_crise')->insert($insert);
        if($input!==false){
          $msg = [
            'id'=>$input->id,
            'dh_registro'=>$input->dh_registro
          ];
          Utils::publishMqtt("quality/ocorrencia-crise/ocorrencia",json_encode($msg));
        }
      }

    }
    return $input;
  }

  protected function processSB($data){
    $input = $this->repository("smartbox")->getInputByIdSbEquipamento($data['id'],$data['num_input']);
    if($input!=null){
      $active = 1;
      $valor = $data['valor'] * 1;
      if($active==$valor){
        $input['descricao'] = strtoupper($input['descricao']);
        $insert = [
          "id_cliente"=>$input['smartbox']['cliente']['id_cliente'],
          "id_box"=>$input['smartbox']['id'],
          "tipo_box"=>$data['tipo'],
          "titulo"=>"DISPARO DE {$input['descricao']}",
          "obs"=>null,
          "dh_registro"=>date("Y-m-d H:i:s"),
          'num_input'=>$data['num_input']
        ];
        $input = $this->repository('ocorrencia_crise')->insert($insert);
        if($input!==false){
          $msg = [
            'id'=>$input->id,
            'dh_registro'=>$input->dh_registro
          ];
          Utils::publishMqtt("quality/ocorrencia-crise/ocorrencia",json_encode($msg));
        }
      }

    }
    return $input;
  }

  public function save(Request $request, Response $response, $args){
    $data = null;
    $data = $request->isPost() ? $request->getParsedBody() : $request->getQueryParams();
    switch($data['tipo']){
      case 'BB':
        $ret = $this->processBB($data);
        break;
      case 'SB':
        $ret = $this->processSB($data);
        break;
    }
    return json_encode($ret);
  }

  public function index(Request $request, Response $response, $args)
  {
    // $this->isLogged($response);
    $ocorrencia = $this->repository("ocorrencia_crise")->show($args['id']);
    return $this->container['view']->render($response, 'ocorrencia/index.twig.php', [
        'ocorrencia' => $ocorrencia['ocorrencia'],
        'contatos' => $ocorrencia['contatos'],
    ]);
  }

  public function abertas(Request $request, Response $response, $args)
  {
    $exists = $this->container['session']->exists('usuario');
    if(!$exists) {
      throw new HttpException(302, ['message' => 'Falha de autenticação', 'code' => '302']);
    }
    $dadosUsuario = $this->container['session']->usuario;
    return json_encode($this->repository("ocorrencia_crise")->getOcorrenciaAberta($dadosUsuario->id_usuario));
  }

  public function finalizar(Request $request, Response $response, $args)
  {
    $exists = $this->container['session']->exists('usuario');
    if(!$exists) {
      throw new HttpException(302, ['message' => 'Falha de autenticação', 'code' => '302']);
    }
    $dadosUsuario = $this->container['session']->usuario;
    $data = $request->getParsedBody();
    $finalizar = $this->repository("ocorrencia_crise")->finalizar($data);
    return json_encode($finalizar);
  }

}