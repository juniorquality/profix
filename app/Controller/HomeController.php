<?php
namespace PROFIX\App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
// use \QCV\Domain\Model\Agenda;

class HomeController extends BaseController
{
    public function index(Request $request, Response $response, $args)
    {
      $isLogged = $this->container['session']->exists('usuario');
      if($isLogged === false) {
          $this->container['flash']->addMessage('message', 'Por favor realize login ');
          return $response->withStatus(200)->withHeader('Location', 'login');
      }

      return $this->renderTemplate('dashboard/index.twig.php', ['sdfadsfas'=>'date()']);
    }

    public function teste(Request $request, Response $response, $args)
    {
      $isLogged = $this->container['session']->exists('usuario');
      if($isLogged === false) {
          $this->container['flash']->addMessage('message', 'Por favor realize login ');
          return $response->withStatus(200)->withHeader('Location', 'login');
      }

      // $$$;

      return $this->container['view']->render($response, 'dashboard/teste.twig.php', [
        'isAjax' => $request->isXhr()
      ]);
    }

    public function sair(Request $request, Response $response, $args)
    {
      $this->container['session']->delete('usuario');
      return $response->withStatus(302)->withHeader('Location', 'login');
    }
}
