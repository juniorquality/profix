<?php
namespace PROFIX\App\Controller;

use \Interop\Container\ContainerInterface;
use \PROFIX\App\Exception\HttpException;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

abstract class BaseController
{
    protected $container;
    protected $elapsed;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->elapsed = microtime(true);
    }

    public function elapsed()
    {
        return round(microtime(true) - $this->elapsed, 6);
    }

    public function jsonResponse($response, $data)
    {
        return $this->container['json.renderer']->render($response, $data);
    }

    public function repository($repository)
    {
        return $this->container["repository.{$repository}"];
    }

    public function renderTemplate($tmpl, $data)
    {
        $data = (array) $data;
        $data['isAjax'] = $this->container['request']->isXhr();
        $data['sessionUsuario'] = $this->container['session']->usuario;
        return $this->container['view']->render(
            $this->container['response'], 
            $tmpl,
            $data
        );
    }
}
