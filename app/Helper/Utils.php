<?php
/**
 * Created by PhpStorm.
 * User: quality
 * Date: 16/08/17
 * Time: 09:29
 */

namespace PROFIX\App\Helper;


use PROFIX\Domain\Model\Smartbox\Relay;

class Utils {
  public static function postUrlRapid($url,$params=array()){
    $content = http_build_query($params);
    $context = stream_context_create(array(
      'http' => array(
        'method'  => 'POST',
        'content' => $content,
      )
    ));
    $contents = file_get_contents($url, null, $context);
    return $contents;
  }
  /**
   * Determine if supplied string is a valid GUID
   *
   * @param string $guid String to validate
   * @return boolean
   */
  public static function isValidGuid($guid){
    return !empty($guid) && preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/', $guid);
  }
  
  
  public static function getNomeRelaySB($output){
    $pattern = "/(\w{1,})\-(\d{1,})\-(\d{1,})\-(\d{1,})\/{0,1}(.*)/";
    $out01 = array();
    preg_match_all($pattern, $output, $out01);
    $out01 = array_filter($out01, function($val) {
      return strlen($val[0]);
    });
    $out01 = array_column($out01, 0);
    try {
      list($dummy, $is_sb, $id_sb, $id_out, $num_relay, $id_qwsensor) = $out01;
    } catch (Exception $e) {
      list($dummy, $is_sb, $id_sb, $id_out, $num_relay) = $out01;
    }
    
    $relay = Relay::find($id_out)->toArray();
    
    return isset($relay['descricao']) ? $relay['descricao'] : "";
    
  }
  
  
  public static function formatFields($string,$type="") {
    $output = trim(preg_replace("[' '-./ t]", '', str_replace(array('(',')'),'',$string)));
    switch(strtolower($type)){
      case 'cpf':
        return preg_replace(
          "/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})/i", "$1.$2.$3-$4", $output
        );
        break;
      case 'cnpj':
        return preg_replace(
          "/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})/i", "$1.$2.$3/$4-$5", $output
        );
        break;
      case 'cep':
        return preg_replace(
          "/([0-9]{5})([0-9]{3})/i", "$1-$2", $output
        );
        break;
      case 'rg':
        return preg_replace(
          "/([0-9]{2})([0-9]{3})([0-9]{3})([0-9a-zA-Z]{1})/i", "$1.$2.$3-$4", $output
        );
        break;
      case 'tel':
        if(strlen($output)==10){
          return preg_replace(
            "/([0-9]{2})([0-9]{4})([0-9]{4})/i", "($1)$2-$3", $output
          );
        }else if(strlen($output)==11){
          return preg_replace(
            "/([0-9]{2})([0-9]{5})([0-9]{4})/i", "($1)$2-$3", $output
          );
        }
        break;
      case 'ddd':
        if(strlen($output)==10){
          return preg_replace(
            "/([0-9]{2})([0-9]{4})([0-9]{4})/i", "$1", $output
          );
        }else if(strlen($output)==11){
          return preg_replace(
            "/([0-9]{2})([0-9]{5})([0-9]{4})/i", "$1", $output
          );
        }
        break;
      case 'tel_sem_ddd':
        if(strlen($output)==10){
          return preg_replace(
            "/([0-9]{2})([0-9]{4})([0-9]{4})/i", "$2-$3", $output
          );
        }else if(strlen($output)==11){
          return preg_replace(
            "/([0-9]{2})([0-9]{5})([0-9]{4})/i", "$2-$3", $output
          );
        }
        break;
    }
    return $string;
  }

  public static function publishMqtt($topic,$message){
    $path_root = dirname(__FILE__);
    $DS = DIRECTORY_SEPARATOR;
    $path_root = "{$path_root}{$DS}..{$DS}";
    $path_root = realpath($path_root).$DS;

    $settings = json_decode(file_get_contents("{$path_root}settings.json"));

    $BROKER = $settings->broker_url;
    $PORT = $settings->broker_port;
    $CLIENT_ID = "pubclient_" . getmypid();

    $client = new \Mosquitto\Client($CLIENT_ID);
    $client->connect($BROKER, $PORT, 60);
    $id = $client->publish($topic, $message, 0, false);
    $client->disconnect();
    if(isset($id) && !empty($id)){
      return true;
    }
    return false;
  }
  
}