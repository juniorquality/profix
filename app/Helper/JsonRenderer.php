<?php
namespace PROFIX\App\Helper;

use \Psr\Http\Message\ResponseInterface;

/**
 * JsonRenderer
 *
 * Render JSON view into a PSR-7 Response object
 */
class JsonRenderer
{
    /**
     *
     * @param ResponseInterface $response
     * @param int $statusCode
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function render(ResponseInterface $response, $data, $statusCode = 200)
    {
        $jsonResponse = $response->withHeader('Content-Type', 'application/json')
            ->withStatus($statusCode);

        $jsonResponse->getBody()->write(json_encode($data));
        return $jsonResponse;
    }
}
