<?php
namespace PROFIX\App\Ext;

use \Yajra\Oci8\Oci8Connection as YajraConnection;

final class Oci8Connection extends YajraConnection
{
    public function setName($name)
    {
        $this->config['name'] = $name;
    }
}
