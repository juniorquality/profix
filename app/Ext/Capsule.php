<?php
namespace PROFIX\App\Ext;

use \PDO;
use \Illuminate\Container\Container;
use \Illuminate\Database\Capsule\Manager;

class Capsule extends Manager
{
    protected $nameConnection;

    public function setNameConnection($nameConnection)
    {
        $this->nameConnection = $nameConnection;
        $this->setupDefaultConfiguration();
        $this->setupManager();
    }

    public function __construct(Container $container = null, $nameConnection = 'default')
    {
        $this->setupContainer($container ?: new Container);
    }

    protected function setupDefaultConfiguration()
    {
        $this->container['config']['database.fetch'] = PDO::FETCH_OBJ;
        $this->container['config']['database.default'] = $this->nameConnection;
    }
}
