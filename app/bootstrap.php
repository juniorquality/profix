<?php
include __DIR__ . '/../vendor/autoload.php';

define('SETTINGS_FILE', __DIR__ . '/settings.json');

if (!function_exists('pr')) {
    function pr() {
        $args = func_get_args();

        foreach ($args as $arg) {
            echo implode('', [
                '<div style="border: solid 1px #000; background-color: #eee;margin: 10px 0px;padding: 10px 15px;"><pre>',
                print_r($arg, true),
                '</pre></div>'
            ]);
        }
    }
}

final class Bootstrap
{
    private static $app = null;
    private static $settings = null;

    public static function init()
    {
        if (empty(self::$app)) {

            session_start();
            $app = new \Slim\App([
                'settings' => [
                    'determineRouteBeforeAppMiddleware' => true,
                    'displayErrorDetails' => true,
                    'debug'=>true
                ]
            ]);

            // $container = $app->getContainer();

            // $errorHandler = function ($request, $response, $exception) use ($c) {
            //     pr($exception);
            //     return $c['response']->withStatus(500)
            //      ->withHeader('Content-Type', 'text/html')
            //      ->write('Something went wrong!');
            // };

            // $container['errorHandler'] = function ($c) use($errorHandler) {
            //     return $errorHandler;
            // };

            // $container['phpErrorHandler'] = function ($c) use($errorHandler) {
            //     return $errorHandler;
            // };

            self::loadSettings();

            $container = $app->getContainer();
            $container['database'] = self::$settings->database;

            include __DIR__ . '/repositories.php';
            include __DIR__ . '/services.php';
            include __DIR__ . '/routes.php';

            self::loadConnections($container);

            $container['flash'] = function () {
                return new \Slim\Flash\Messages();
            };

            $container['session'] = function ($c) {
                return new \SlimSession\Helper;
            };

            $container['view'] = function ($container) {
                $view = new \Slim\Views\Twig('app/Templates', [
                    // 'cache' => 'app/Cache'
                ]);

                // Instantiate and add Slim specific extension
                $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
                $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

                $function = new Twig_SimpleFunction('baseUrl', function ($url = null) {
                    return self::$settings->base_url . ( !isset($url) || !is_null($url) ? $url : '' );
                });
                $view->getEnvironment()->addFunction($function);

                $flash = $container['flash'];
                $function = new Twig_SimpleFunction('getMessagesFlashHTML', function () use ($flash) {
                    $msgs = $flash->getMessages('message');
                    return isset($msgs['message']) ? $msgs['message'] : '';
                });
                $view->getEnvironment()->addFunction($function);

                $isAjax = $container['request']->isXhr();
                $function = new Twig_SimpleFunction('isAjax', function () use ($isAjax) {
                    return $isAjax;
                });
                $view->getEnvironment()->addFunction($function);

                return $view;
            };

            self::$app = $app;
        }

        return self::$app;
    }

    private static function loadSettings()
    {
        if (!is_readable(SETTINGS_FILE)) {
            throw new \Exception("Impossível ler arquivo settings.json");
        }

        self::$settings = json_decode(file_get_contents(SETTINGS_FILE));
    }

    private static function loadConnections($container)
    {
        $capsule = new \PROFIX\App\Ext\Capsule();

        foreach ((array) $container['database'] as $connectionName => $connectionConfig) {
            $capsule->addConnection((array) $connectionConfig, $connectionName);

            if ($connectionConfig->driver == 'oracle') {
                $capsule->setNameConnection($connectionName);
            }

            if ($connectionConfig->driver == 'sqlsrv') {
                $conn = $capsule->getConnection($connectionName);
                $conn->getPDO()->setAttribute(\PDO::SQLSRV_ATTR_QUERY_TIMEOUT, 10);
            }
        }

        $capsule->getDatabaseManager()->extend('oracle', function ($config) {
            $connector = new Yajra\Oci8\Connectors\OracleConnector();
            $connection = $connector->connect($config);
            $db = new PROFIX\App\Ext\Oci8Connection($connection, $config['database'], $config['prefix']);
            $sessionVars = [
                'NLS_TIME_FORMAT' => 'HH24:MI:SS',
                'NLS_DATE_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
                'NLS_NUMERIC_CHARACTERS' => '.,',
            ];

            if (isset($config['schema'])) {
                $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
            }

            $db->setSessionVars($sessionVars);
            return $db;
        });

        $capsule->bootEloquent();
        $capsule->setAsGlobal();


        foreach ((array) $container['database'] as $connectionName => $connectionConfig) {
            if ($connectionConfig->driver == 'oracle') {
                $capsule->getDatabaseManager()->connection($connectionName)->setName($connectionName);
            }
        }



    }
}
