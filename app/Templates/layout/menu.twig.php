<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="profile-image"> <img src="{{baseUrl()}}assets/images/faces/face4.jpg" alt="image"/> <span class="online-status online"></span> </div>
        <div class="profile-name">
          <p class="name">{{sessionUsuario.nome}}</p>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link linkAjax" href="{{ baseUrl() }}">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/01.png" alt="menu icon">
        <span class="menu-title">Dashboard</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/08.png" alt="menu icon">
        <span class="menu-title">Equipamento</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="general-pages">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ baseUrl('equipamento') }}">Equipamento</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ baseUrl('problema') }}">Problema</a></li>
        </ul>
      </div>
    </li>
    <!-- <li class="nav-item">
      <a class="nav-link linkAjax" href="{{ baseUrl() }}teste">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/02.png" alt="menu icon">
        <span class="menu-title">Widgets</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../pages/ui-features/buttons.html">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/03.png" alt="menu icon">
        <span class="menu-title">Buttons</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../pages/forms/basic_elements.html">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/04.png" alt="menu icon">
        <span class="menu-title">Form</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../pages/charts/chartjs.html">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/05.png" alt="menu icon">
        <span class="menu-title">Charts</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../pages/tables/basic-table.html">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/06.png" alt="menu icon">
        <span class="menu-title">Table</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../pages/icons/font-awesome.html">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/07.png" alt="menu icon">
        <span class="menu-title">Icons</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/08.png" alt="menu icon">
        <span class="menu-title">General Pages</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="general-pages">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> a class="nav-link" href="../pages/samples/blank-page.html">Blank Page</a></li>
          <li class="nav-item"> <a class="nav-link" href="../pages/samples/login.html">Login</a></li>
          <li class="nav-item"> <a class="nav-link" href="../pages/samples/register.html">Register</a></li>
          <li class="nav-item"> <a class="nav-link" href="../pages/samples/error-404.html">404</a></li>
          <li class="nav-item"> <a class="nav-link" href="../pages/samples/error-500.html">500</a></li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../pages/ui-features/typography.html">
        <img class="menu-icon" src="{{baseUrl()}}assets/images/menu_icons/09.png" alt="menu icon">
        <span class="menu-title">Typography</span>
      </a>
    </li> -->
  </ul>
</nav>