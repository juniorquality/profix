<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>{% block title %}{% endblock %}</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{baseUrl()}}node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="{{baseUrl()}}node_modules/simple-line-icons/css/simple-line-icons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="{{baseUrl()}}node_modules/font-awesome/css/font-awesome.min.css" />
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{baseUrl()}}assets/css/pace.css">
  <link rel="stylesheet" href="{{baseUrl()}}assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{baseUrl()}}assets/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    {% include 'layout/header.twig.php' %}
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../partials/_sidebar.html -->
      {% include 'layout/menu.twig.php' %}
      <!-- partial -->
      <div class="main-panel">
        <div id="content-main">
        {% block content %}{% endblock %}
        </div>
        <!-- content-wrapper ends -->
        
        {% include 'layout/footer.twig.php' %}
        

      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{baseUrl()}}node_modules/jquery/dist/jquery.min.js"></script>
  <script src="{{baseUrl()}}node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="{{baseUrl()}}node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="{{baseUrl()}}assets/js/pace.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5NXz9eVnyJOA81wimI8WYE08kW_JMe8g&callback=initMap" async defer></script> -->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{baseUrl()}}assets/js/off-canvas.js"></script>
  <script src="{{baseUrl()}}assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- <script src="{{baseUrl()}}assets/js/maps.js"></script> -->

  <script src="{{baseUrl()}}assets/js/main.js"></script>
  <!-- End custom js for this page-->


  {% block scripts %}{% endblock %}

</body>

</html>