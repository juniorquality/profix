<!doctype html>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="assets/img/favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>INDEX</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{baseUrl()}}assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="{{baseUrl()}}assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{baseUrl()}}assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
</head>
<body>
  <!-- <h1></h1> -->
  <!-- {{ ocorrencia }} -->
  <!-- {{ contatos }} -->
  <!-- {{baseUrl()}} -->
  <div class="wrapper">
    <div class="main-panel">
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="header">
                    <h4 class="title">[{{ ocorrencia.id }}] Ocorrência - {{ ocorrencia.titulo|upper  }}</h4>
                </div>
                <div class="content">
                  <form method="post" id="frmOcorrencia">
                    <input type="hidden" id="id" name="id" value="{{ ocorrencia.id }}">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>Empresa</label>
                          <input type="text" class="form-control" disabled placeholder="Empresa" value="{{ ocorrencia.nome_cliente }}">
                        </div>
                      </div>

                      <div class="col-md-5">
                        <div class="form-group">
                          <label>Contatos</label>
                          <select class="form-control" name="id_contato" id="id_contato">
                            <option value="">SELECIONE</option>
                            {% for contato in contatos %}
                            <option value="{{contato.id}}" data-telefone="{{contato.ddd}}{{contato.telefone}}">{{contato.nome}} - ({{contato.ddd}}) {{contato.telefone}}</option>
                            {% endfor %}
                          </select>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="form-group">
                          <label>Telefone</label>
                          <input type="text" class="form-control" placeholder="Telefone" id="telefone">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Observação</label>
                          <textarea rows="5" class="form-control" name="obs" id="obs" placeholder="Informe uma observação para a ocorrência"></textarea>
                        </div>
                      </div>
                    </div>

                    <button type="button" id="salvar" class="btn btn-info btn-fill pull-right">Salvar ocorrência</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="{{baseUrl()}}assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
  <script src="{{baseUrl()}}assets/js/bootstrap.min.js" type="text/javascript"></script>
  <script>
    jQuery(document).ready(function($) {
      $('#id_contato').change(function() {
        var data = $('option[value='+$(this).val()+']').data();
        var $temp = $("<input>");
        var tel = data.telefone;
        $('#telefone').val(tel)
        // $("body").append($temp);
        // $temp.val(tel).select();
        // document.execCommand("Copy");
        // $temp.remove();
      });
      $("#salvar").click(function(event) {

        if($('#id_contato').val() == '') {
          alert('Selecione um contato!');
          return;
        }
        if($('#obs').val() == '') {
          alert('Informe uma observação!');
          return;
        }

        $.ajax({
          url: '{{baseUrl()}}ocorrencia/finalizar',
          type: 'POST',
          dataType: 'json',
          data: $("#frmOcorrencia").serializeArray(),
        })
        .always(function() {
          parent.fechaOcorrencia();
        });
        
      });
    });
  </script>
</body>
</html>
<!-- {{ ocorrencia }} -->