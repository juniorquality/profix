<!DOCTYPE html>
<html>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>LOGIN</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{baseUrl()}}node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="{{baseUrl()}}node_modules/simple-line-icons/css/simple-line-icons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{baseUrl()}}assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{baseUrl()}}assets/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth login-full-bg">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-dark text-left p-5">
              <h2>Login</h2>
              <!-- <h4 class="font-weight-light">Hello! let's get started</h4> -->

              {% if getMessagesFlashHTML() %}
              <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                {% for msg in getMessagesFlashHTML() %}
                  {{ msg|e }}
                {% endfor %}
              </div>
              {% endif %}

              <form class="form-signin" method="POST" action="{{ baseUrl() }}login">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="text" class="form-control" name="login" required="" autofocus="" />
                  <i class="mdi mdi-account"></i>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Senha</label>
                  <input type="password" class="form-control" name="senha" required=""/>
                  <i class="mdi mdi-eye"></i>
                </div>
                <div class="mt-5">
                  <button class="btn btn-block btn-warning btn-lg font-weight-medium" type="submit">Login</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{baseUrl()}}node_modules/jquery/dist/jquery.min.js"></script>
  <script src="{{baseUrl()}}node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="{{baseUrl()}}node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{baseUrl()}}assets/js/off-canvas.js"></script>
  <script src="{{baseUrl()}}assets/js/misc.js"></script>
  <!-- endinject -->
</body>

</html>
